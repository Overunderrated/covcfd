# Exercise 0: Time integration with explicit Runge-Kutta methods

- **What you'll write:** Time integrators capable of time-marching problems with arbitrary forcing functions ("right-hand-sides").
- **[Theory reading suggestion, particularly the derivation from Taylor series.](http://web.mit.edu/10.001/Web/Course_Notes/Differential_Equations_Notes/lec24.html)**

Most physical problems of interest have some time dependence (they are *unsteady*); even those that do not (*steady* problems) can be recast and solved as a time-dependent problem. This exercise is to numerically approximate the solution of the general vector-valued ordinary differential equation (ODE)
```math
\frac{d\mathbf{u}}{dt} = \mathbf{f}(t,\mathbf{u})
```
subject to the initial condition
```math
\mathbf{u}(t=t_0) = \mathbf{u}_0
```
using *explicit Runge-Kutta methods*.


## Explicit Runge-Kutta algorithm

- [Wikipedia section on explicit Runge-Kutta](https://en.wikipedia.org/wiki/Runge%E2%80%93Kutta_methods#Explicit_Runge%E2%80%93Kutta_methods)
- [Handy list of Butcher tableaus for Runge-Kutta methods](https://en.wikipedia.org/wiki/List_of_Runge%E2%80%93Kutta_methods)

## What to implement

- Time integrators of various orders, suggesting forward Euler (RK1), midpoint method (RK2), Third-order Strong Stability Preserving Runge-Kutta (SSPRK3) or Ralston's 3rd order method (RK3), classical RK4.
    - Bonus: try [linear multi-step methods like Adams-Bashforth \(AB2\)](https://en.wikipedia.org/wiki/Linear_multistep_method)
- Various "right hand sides" for the time integrators, described in the following problems. 

## Solutions in C++

- [Problem 1: An initial pass](p0_problem1.md)
- [Problem 1: Refactoring the time integrators](p0_problem1_refactoring.md)
- [Problem 1: Refactoring the time integrators part 2](p0_problem1_refactoring2.md)
- [Problem 2: An initial pass](p0_problem2.md)
- [Problem 2: Implementing the observer pattern](p0_problem2_observer.md)
- [Problem 2: Cleanup](p0_problem2_cleanup.md)

## Problem 1 - Scalar-valued ODE

Solve the first-order linear scalar ODE
```math
\frac{du}{dt} = s(t)
```
with the source term
```math
s(t) = 12 - 20t + 30 \pi t^2 \cos{(20 \pi t^3)}
```


with initial condition $`u(t=0) = 1`$ in the time interval $`t_0 = 0`$ to $`t_f = 1`$ using the time integrators.

This problem has the exact solution
```math
u_{exact}(t) = 1+ 12t - 10 t^2 + 0.5 \sin{(20 \pi t^3)}
```

<details>
<summary>Click to expand example solution plot</summary>
![alt text](levequesoln.png)
</details>


Define your relative error as
```math
r = \left| \frac{u_{computed} - u_{exact}}{u_{exact}} \right|
```

### Outputs:

- **Solve with your forward Euler (RK1) integrator using varying time step sizes.**
    - Use decreasing step sizes (increasing resolution) of $`\Delta t = t_f/n_\text{steps}`$ for $`n_\text{steps} = 10^2, 10^3, 10^4`$ and record the error.
    - Plot the error $`r`$ vs step size $`\Delta t`$ on a log-log plot, and ensure that the error decreases with decreasing step size.
- **Repeat with your other time integrators and plot them alongside.**
    - How do the errors compare at the same time steps?
    - What are the slopes of these functions?

![alt text](levequeconvergence.png)

### Implications:

- **Imagine your problem can tolerate a given level of error.**
    - What time step size / how many steps do you need to use for each of these methods to achieve 1% error? 0.1%?
- **Imagine you have a fixed amount of computer power available and can only run a set number of timesteps.**
    - If you can only run 100 time steps, what level of error can you expect with the different methods? If you can run 1000 or 10000?

## Problem 2 - Vector-valued ODE

Solve the [Lotka-Volterra equations](https://en.wikipedia.org/wiki/Lotka%E2%80%93Volterra_equations),
```math
\frac{dx}{dt} = \alpha x - \beta x y \\

\frac{dy}{dt} = \delta x y - \gamma y
```
with $`\alpha=1.1`$, $`\beta=0.4`$, $`\gamma=0.1`$, $`\delta=0.4`$, and initial conditions of $`\{10,10\}`$ from time $`t_0=0`$ to $`t_f=1000`$. The numerical solution should look like this:

![Lotka-Volterra solution](lotkavolterrasoln.png)

This is a nonlinear dynamical system and it lacks the nice exact solutions of the previous problem. There's always a way to assess accuracy however, sometimes you just need to get creative. In this case there is a conserved constant
```math
V = \delta x - \gamma \log{x} + \beta y - \alpha \log{y}
```
which can be used to track error over time. These initial conditions give an initial value of $`V_0 \approx 5.23689788841`$. Plotting the normalized value of $`V_{computed} / V_0`$ using RK4 with 10,000 time steps gives the following history of the quantity that *should* be conserved:

![Lotka-Volterra V](lotkavolterraVrk4.png)

After $`t_f=1000`$ about 1% has been lost to numerical error.

### Outputs:

- **As above, solve this vector-valued function with your time integrators, varying the time step.**
    - Plot the relative error of the conserved constant $`V`$ over time to $`t_f`$ for each method using $`10^5`$ time steps
    - Plot the relative error in the value of the conserved constant $`V`$ at the final time $`t_f`$ for each method, while varying the time step size.

![Lotka-Volterra V](lotkavolterra_conservationvstime.png)




## Miscellaneous related things to be aware of that weren't mentioned

These simple explicit RK methods with constant time step sizes are far from the only methods for time integration. In fact, in "real engineering" CFD you might almost never use them; explicit methods have massive drawbacks related to speed and stability, so implicit methods are preferred for most cases.

- [Runge-Kutta methods with "embedded" formulae](https://en.wikipedia.org/wiki/List_of_Runge%E2%80%93Kutta_methods#Embedded_methods) give the ability to include error estimates at each time step, typically used in conjunction with adaptive time-stepping to control that error.
- [Backward differentiation formulas (BDFs)](http://www.scholarpedia.org/article/Backward_differentiation_formulas) are very popular in engineering CFD for coupled/compressible methods.
- [Fractional step or projection methods](https://en.wikipedia.org/wiki/Projection_method_(fluid_dynamics)) are popular for incompressible CFD.
- [Implicit Runge-Kutta methods](https://en.wikipedia.org/wiki/Runge%E2%80%93Kutta_methods#Implicit_Runge%E2%80%93Kutta_methods) are used where very high order combined with large time steps is required, mostly in research codes.

## Book references
- "Solving Ordinary Differential Equations I" - Hairer, Norsett, & Wanner, chapter 2.
- "Ordinary Differential Equations" - Tenenbaum & Pollard
- "Numerical Initial Value Problems in Ordinary Differential Equations" - C. William Gear
