# Problem 1: Refactoring the time integrators

- [Problem 1: An initial pass](p0_problem1.md)
- [Problem 1: Refactoring the time integrators](p0_problem1_refactoring.md)
- [Problem 1: Refactoring the time integrators part 2](p0_problem1_refactoring2.md)
- [Problem 2: An initial pass](p0_problem2.md)
- [Problem 2: Implementing the observer pattern](p0_problem2_observer.md)
- [Problem 2: Cleanup](p0_problem2_cleanup.md)

Run
```
git checkout v0.1.2
```
to fast-forward to the code at the end of this section.


## Finding commonality

Referring back to [the initial implementation](p0_problem1.md), let's look for places to improve. Looking at the implementations of `forward_euler` and `rk4`, there is a considerable amount of common code. Consider the `forward_euler` implementation where we comment out the unique code and leave the code that is shared with `rk4`:

```c++
Eigen::VectorXd forward_euler(const RHSFunction& rhs, const Eigen::VectorXd& u0, double t0, double tf, double dt)
{
//   Eigen::VectorXd u    = u0;
//   double          time = t0;
//   Eigen::VectorXd dudt;

  while (time < tf) {
    // rhs(u, time, dudt);

    if (time + dt >= tf) {
      dt   = tf - time;
      time = tf;
    } else {
      time += dt;
    }

    u += dt * dudt;
  }

  return u;
}
```

The flow control structure of these functions are identical, and the only differ in how they compute the update `dudt`. Continuing this pattern means we would be duplicating code for every other integrator we implement, increasing the likelihood of implementation bugs.


## An OOP solution with an ExplicitRungeKutta base class

One way to make this common code re-usable and less redundant (by no means the only way) is through basic object oriented programming using inheritance. Consider a base class which only implements the common control structure, and requires derived classes to implement the details of computing the update:

```c++
class ExplicitRungeKutta
{
public:
  Eigen::VectorXd
  solve(const RHSFunction& rhs, const Eigen::VectorXd& u0, double t0, double tf, double dt)
  {
    Eigen::VectorXd u    = u0;
    double          time = t0;
    Eigen::VectorXd dudt;

    // sanity check for overly large dt inputs
    if (time + dt >= tf)
      dt = tf - time;

    while (time < tf) {
      bool last_step = false;
      if (time + dt >= tf) {
        dt        = tf - time;
        last_step = true;
      }

      step(rhs, u, time, dt, dudt);

      time += dt;
      u += dt * dudt;

      if (last_step)
        break;
    }

    return u;
  }

private:
  virtual void step(
    const RHSFunction&     rhs,
    const Eigen::VectorXd& u,
    double                 time,
    double                 dt,
    Eigen::VectorXd&       dudt) = 0;
};
```


## Concrete time integrator implementations

Now all the derived classes have to implement is the `step` function. Forward Euler becomes:

```c++
class ForwardEuler : public ExplicitRungeKutta
{
private:
  void step(
    const RHSFunction&     rhs,
    const Eigen::VectorXd& u,
    double                 time,
    double                 dt,
    Eigen::VectorXd&       dudt) override
  {
    rhs(u, time, dudt);
  }
};
```

and RK4 becomes:
```c++
class RK4 : public ExplicitRungeKutta
{
private:
  void step(
    const RHSFunction&     rhs,
    const Eigen::VectorXd& u,
    double                 time,
    double                 dt,
    Eigen::VectorXd&       dudt) override
  {
    rhs(u, time, k1);
    rhs(u + 0.5 * k1, time + 0.5 * dt, k2);
    rhs(u + 0.5 * k2, time + 0.5 * dt, k3);
    rhs(u + k3, time + dt, k4);
    dudt = 1. / 6. * (k1 + 2. * k2 + 2. * k3 + k4);
  }

  Eigen::VectorXd k1, k2, k3, k4;
};
```

Adding new integrators in this fashion now requires less redundant code (boilerplate), and all the implementation specifics of the concrete integrators are private.




<details>
<summary><b>Click to expand implementation of the second-order midpoint method</b></summary>

```c++
class RK2Midpoint : public ExplicitRungeKutta
{
private:
  void step(
    const RHSFunction&     rhs,
    const Eigen::VectorXd& u,
    double                 time,
    double                 dt,
    Eigen::VectorXd&       dudt) override
  {
    rhs(u, time, k1);
    rhs(u + 0.5 * k1, time + 0.5 * dt, dudt);
  }
  Eigen::VectorXd k1;
};
```

</details>


<details>
<summary><b>Click to expand implementation of the strong-stability preserving RK3 (RK3SSP)</b></summary>

```c++

class RK3SSP : public ExplicitRungeKutta
{
private:
  void step(
    const RHSFunction&     rhs,
    const Eigen::VectorXd& u,
    double                 time,
    double                 dt,
    Eigen::VectorXd&       dudt) override
  {
    rhs(u, time, k1);
    rhs(u+dt*a21*k1, time+c2*dt, k2);
    rhs(u+dt*(a31*k1+a32*k2), time+c3*dt, k3);
    dudt = b1*k1 + b2*k2 + b3*k3;
  }

  Eigen::VectorXd  k1, k2, k3;
  static constexpr double c2  = 0.5;
  static constexpr double c3  = 0.75;
  static constexpr double a21 = 0.5;
  static constexpr double a31 = 0.0;
  static constexpr double a32 = 0.74;
  static constexpr double b1  = 2. / 9.;
  static constexpr double b2  = 1. / 3.;
  static constexpr double b3  = 4. / 9.;
};
```

</details>


Extending our `int main()` to use these new functions


<details>
<summary><b>Click to expand the updated main function</b></summary>

```c++
int main()
{
  plotScalarODEExactSolution();

  const Eigen::VectorXd u0 = Eigen::VectorXd::Constant(1, 1.0);
  const double          t0 = 0;
  const double          tf = 1;

  const double exact_soln = scalarODEExactSolution(tf);

  const std::vector<double> timestepsize{ 1. / 100., 1. / 1000., 1. / 10000. };
  std::vector<double>       rk4_err;
  std::vector<double>       rk3_err;
  std::vector<double>       rk2_err;
  std::vector<double>       forward_euler_err;

  for (const auto dt : timestepsize) {
    forward_euler_err.push_back(
      relative_error(exact_soln, ForwardEuler().solve(scalarODERightHandSide, u0, t0, tf, dt)[0]));
    rk4_err.push_back(
      relative_error(exact_soln, RK4().solve(scalarODERightHandSide, u0, t0, tf, dt)[0]));
    rk3_err.push_back(
      relative_error(exact_soln, RK3SSP().solve(scalarODERightHandSide, u0, t0, tf, dt)[0]));
    rk2_err.push_back(
      relative_error(exact_soln, RK2Midpoint().solve(scalarODERightHandSide, u0, t0, tf, dt)[0]));
  }

  plt::figure();
  plt::named_loglog("RK1 (Forward Euler)", timestepsize, forward_euler_err, "-o");
  plt::named_loglog("RK2", timestepsize, rk2_err, "-o");
  plt::named_loglog("RK3", timestepsize, rk3_err, "-o");
  plt::named_loglog("RK4", timestepsize, rk4_err, "-o");
  plt::legend();
  plt::grid(true);
  plt::xlabel("$\\Delta t$");
  plt::ylabel("Error");
  plt::show();
}
```

</details>

and hopefully you see convergence like this:


![alt text](levequeconvergence_1through4.png)


## Full solution

<details>
<summary><b>Click to expand the full code for the first problem</b></summary>


```c++
#include <Eigen/Dense>
#include <cmath>
#include <matplotlibcpp.h>
#include <vector>

namespace plt     = matplotlibcpp;
using RHSFunction = std::function<void(const Eigen::VectorXd&, double, Eigen::VectorXd&)>;

std::vector<double>
EigenToStdVec(const Eigen::VectorXd& e)
{
  std::vector<double> v(e.rows());
  for (long i = 0; i < e.rows(); ++i) {
    v[i] = e[i];
  }
  return v;
}

double
scalarODEExactSolution(double t)
{
  return 1 + 12 * t - 10 * t * t + 0.5 * sin(20 * M_PI * std::pow(t, 3));
}

void
plotScalarODEExactSolution()
{
  const Eigen::VectorXd t = Eigen::VectorXd::LinSpaced(1000, 0, 1);
  plt::figure();
  plt::plot(EigenToStdVec(t), [](double t) { return scalarODEExactSolution(t); }, "k-");
  plt::xlabel("$t$");
  plt::ylabel("$u(t)$");
}

void
scalarODERightHandSide(const Eigen::VectorXd& u, double t, Eigen::VectorXd& dudt)
{
  assert(u.size() == 1);
  dudt.resize(u.size());
  dudt[0] = 12 - 20 * t + 30 * M_PI * t * t * cos(20 * M_PI * std::pow(t, 3));
}

class ExplicitRungeKutta
{
public:
  Eigen::VectorXd
  solve(const RHSFunction& rhs, const Eigen::VectorXd& u0, double t0, double tf, double dt)
  {
    Eigen::VectorXd u    = u0;
    double          time = t0;
    Eigen::VectorXd dudt;

    // sanity check for overly large dt inputs
    if (time + dt >= tf)
      dt = tf - time;

    while (time < tf) {
      bool last_step = false;
      if (time + dt >= tf) {
        dt        = tf - time;
        last_step = true;
      }

      step(rhs, u, time, dt, dudt);

      time += dt;
      u += dt * dudt;

      if (last_step)
        break;
    }

    return u;
  }

private:
  virtual void step(
    const RHSFunction&     rhs,
    const Eigen::VectorXd& u,
    double                 time,
    double                 dt,
    Eigen::VectorXd&       dudt) = 0;
};

class ForwardEuler : public ExplicitRungeKutta
{
private:
  void step(
    const RHSFunction&     rhs,
    const Eigen::VectorXd& u,
    double                 time,
    double                 dt,
    Eigen::VectorXd&       dudt) override
  {
    rhs(u, time, dudt);
  }
};

class RK4 : public ExplicitRungeKutta
{
private:
  void step(
    const RHSFunction&     rhs,
    const Eigen::VectorXd& u,
    double                 time,
    double                 dt,
    Eigen::VectorXd&       dudt) override
  {
    rhs(u, time, k1);
    rhs(u + 0.5 * k1, time + 0.5 * dt, k2);
    rhs(u + 0.5 * k2, time + 0.5 * dt, k3);
    rhs(u + k3, time + dt, k4);
    dudt = 1. / 6. * (k1 + 2. * k2 + 2. * k3 + k4);
  }

  Eigen::VectorXd k1, k2, k3, k4;
};

class RK2Midpoint : public ExplicitRungeKutta
{
private:
  void step(
    const RHSFunction&     rhs,
    const Eigen::VectorXd& u,
    double                 time,
    double                 dt,
    Eigen::VectorXd&       dudt) override
  {
    rhs(u, time, k1);
    rhs(u + 0.5 * k1, time + 0.5 * dt, dudt);
  }
  Eigen::VectorXd k1;
};

class RK3SSP : public ExplicitRungeKutta
{
private:
  void step(
    const RHSFunction&     rhs,
    const Eigen::VectorXd& u,
    double                 time,
    double                 dt,
    Eigen::VectorXd&       dudt) override
  {
    rhs(u, time, k1);
    rhs(u+dt*a21*k1, time+c2*dt, k2);
    rhs(u+dt*(a31*k1+a32*k2), time+c3*dt, k3);
    dudt = b1*k1 + b2*k2 + b3*k3;
  }

  Eigen::VectorXd  k1, k2, k3;
  static constexpr double c2  = 0.5;
  static constexpr double c3  = 0.75;
  static constexpr double a21 = 0.5;
  static constexpr double a31 = 0.0;
  static constexpr double a32 = 0.74;
  static constexpr double b1  = 2. / 9.;
  static constexpr double b2  = 1. / 3.;
  static constexpr double b3  = 4. / 9.;
};

double
relative_error(double expected, double computed)
{
  return abs(computed / expected - 1.0);
}

int
main()
{
  plotScalarODEExactSolution();

  const Eigen::VectorXd u0 = Eigen::VectorXd::Constant(1, 1.0);
  const double          t0 = 0;
  const double          tf = 1;

  const double exact_soln = scalarODEExactSolution(tf);

  const std::vector<double> timestepsize{ 1. / 100., 1. / 1000., 1. / 10000. };
  std::vector<double>       rk4_err;
  std::vector<double>       rk3_err;
  std::vector<double>       rk2_err;
  std::vector<double>       forward_euler_err;

  for (const auto dt : timestepsize) {
    forward_euler_err.push_back(
      relative_error(exact_soln, ForwardEuler().solve(scalarODERightHandSide, u0, t0, tf, dt)[0]));
    rk4_err.push_back(
      relative_error(exact_soln, RK4().solve(scalarODERightHandSide, u0, t0, tf, dt)[0]));
    rk3_err.push_back(
      relative_error(exact_soln, RK3SSP().solve(scalarODERightHandSide, u0, t0, tf, dt)[0]));
    rk2_err.push_back(
      relative_error(exact_soln, RK2Midpoint().solve(scalarODERightHandSide, u0, t0, tf, dt)[0]));
  }

  plt::figure();
  plt::named_loglog("RK1 (Forward Euler)", timestepsize, forward_euler_err, "-o");
  plt::named_loglog("RK2", timestepsize, rk2_err, "-o");
  plt::named_loglog("RK3", timestepsize, rk3_err, "-o");
  plt::named_loglog("RK4", timestepsize, rk4_err, "-o");
  plt::legend();
  plt::grid(true);
  plt::xlabel("$\\Delta t$");
  plt::ylabel("Error");
  plt::show();
}
```

</details>