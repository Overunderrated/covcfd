# Problem 1: Refactoring the time integrators part 2

- [Problem 1: An initial pass](p0_problem1.md)
- [Problem 1: Refactoring the time integrators](p0_problem1_refactoring.md)
- [Problem 1: Refactoring the time integrators part 2](p0_problem1_refactoring2.md)
- [Problem 2: An initial pass](p0_problem2.md)
- [Problem 2: Implementing the observer pattern](p0_problem2_observer.md)
- [Problem 2: Cleanup](p0_problem2_cleanup.md)

Run
```
git checkout v0.1.3
```
to fast-forward to the code at the end of this section.


## Defining the Runge-Kutta methods with Butcher tableaus

[Looking at the RK4 example from earlier](p0_problem1_refactoring.md), we are still conceptually repeating some logic. Namely that each integrator is still calling the `RHS` function directly, and doing its own arithmetic operations with the results to compute the update.

An even more concise way the differences between the Runge-Kutta methods can be defined is [the Butcher tableau](https://en.wikipedia.org/wiki/List_of_Runge%E2%80%93Kutta_methods) where the matrix $`A`$ and vectors $`b`$ and $`c`$ fully define the method.

Modify the base `ExplicitRungeKutta` class such that the concrete derived classes are only responsible for providing the coefficients of the Butcher tableau:

```c++
class ExplicitRungeKutta
{
public:
  // ...
private:
  // removing: virtual void step( ... )  
  virtual Eigen::MatrixXd ButcherTableauA() const = 0;
  virtual Eigen::VectorXd ButcherTableauB() const = 0;
  virtual Eigen::VectorXd ButcherTableauC() const = 0;
};
```

and then implement the `solve` function from `ExplicitRungeKutta` such that it only depends on the Butcher tableau coefficients:

```c++
class ExplicitRungeKutta
{
public:
  Eigen::VectorXd
  solve(const RHSFunction& rhs, const Eigen::VectorXd& u0, double t0, double tf, double dt)
  {
    const auto A        = ButcherTableauA();
    const auto b        = ButcherTableauB();
    const auto c        = ButcherTableauC();
    const int  n_stages = A.rows();

    std::vector<Eigen::VectorXd> k(n_stages);

    Eigen::VectorXd u    = u0;
    double          time = t0;

    // sanity check for overly large dt inputs
    if (time + dt >= tf)
      dt = tf - time;

    while (time < tf) {

      bool last_step = false;
      if (time + dt >= tf) {
        dt        = tf - time;
        last_step = true;
      }

      for (int istage = 0; istage < n_stages; ++istage) {

        const double    time_at_stage = time + c[istage] * dt;
        Eigen::VectorXd u_at_stage    = u;

        for (int butchercol = 0; butchercol < istage; ++butchercol) {
          u_at_stage += dt * A(istage - 1, butchercol) * k[istage - 1];
        }

        rhs(u_at_stage, time_at_stage, k[istage]);
      }

      for (int istage = 0; istage < n_stages; ++istage) {
        u += dt * b[istage] * k[istage];
      }

      time += dt;

      if (last_step)
        break;
    }

    return u;
  }
// ...
};
```


With this in place, forward Euler can be implemented just by defining its Butcher tableau:

```c++
class ForwardEuler : public ExplicitRungeKutta
{
private:
  Eigen::MatrixXd ButcherTableauA() const override { return Eigen::MatrixXd::Zero(1, 1); }
  Eigen::VectorXd ButcherTableauB() const override { return Eigen::VectorXd::Constant(1, 1.0); }
  Eigen::VectorXd ButcherTableauC() const override { return Eigen::VectorXd::Constant(1, 0.0); }
};
```

and similarly RK4:


<details>
<summary><b>Click to expand RK4 code</b></summary>


```c++
class RK4 : public ExplicitRungeKutta
{
private:
  Eigen::MatrixXd ButcherTableauA() const override
  {
    Eigen::MatrixXd A = Eigen::MatrixXd::Zero(4, 4);
    A(1, 0)           = 0.5;
    A(2, 1)           = 0.5;
    A(3, 2)           = 1.0;

    return A;
  }

  Eigen::VectorXd ButcherTableauB() const override
  {
    Eigen::VectorXd b = Eigen::VectorXd::Zero(4);
    b[0]              = 1. / 6.;
    b[1]              = 1. / 3.;
    b[2]              = 1. / 3.;
    b[3]              = 1. / 6.;
    return b;
  }

  Eigen::VectorXd ButcherTableauC() const override
  {
    Eigen::VectorXd c = Eigen::VectorXd::Zero(4);
    c[1]              = 0.5;
    c[2]              = 0.5;
    c[3]              = 1.0;
    return c;
  }
};
```

</details>


## Full code

The full code listing now can be seen below, where we have also added the RK3 method of Ralston. The integrator classes now define only the absolute bare minimum information needed without any redundancy.

<details>
<summary><b>Click to expand full code listing</b></summary>


```c++
#include <Eigen/Dense>
#include <cmath>
#include <matplotlibcpp.h>
#include <vector>

namespace plt     = matplotlibcpp;
using RHSFunction = std::function<void(const Eigen::VectorXd&, double, Eigen::VectorXd&)>;

std::vector<double>
EigenToStdVec(const Eigen::VectorXd& e)
{
  std::vector<double> v(e.rows());
  for (long i = 0; i < e.rows(); ++i) {
    v[i] = e[i];
  }
  return v;
}

double
scalarODEExactSolution(double t)
{
  return 1 + 12 * t - 10 * t * t + 0.5 * sin(20 * M_PI * std::pow(t, 3));
}

void
plotScalarODEExactSolution()
{
  const Eigen::VectorXd t = Eigen::VectorXd::LinSpaced(1000, 0, 1);
  plt::figure();
  plt::plot(EigenToStdVec(t), [](double t) { return scalarODEExactSolution(t); }, "k-");
  plt::xlabel("$t$");
  plt::ylabel("$u(t)$");
}

void
scalarODERightHandSide(const Eigen::VectorXd& u, double t, Eigen::VectorXd& dudt)
{
  assert(u.size() == 1);
  dudt.resize(u.size());
  dudt[0] = 12 - 20 * t + 30 * M_PI * t * t * cos(20 * M_PI * std::pow(t, 3));
}

class ExplicitRungeKutta
{
public:
  Eigen::VectorXd
  solve(const RHSFunction& rhs, const Eigen::VectorXd& u0, double t0, double tf, double dt)
  {
    const auto A        = ButcherTableauA();
    const auto b        = ButcherTableauB();
    const auto c        = ButcherTableauC();
    const int  n_stages = A.rows();

    std::vector<Eigen::VectorXd> k(n_stages);

    Eigen::VectorXd u    = u0;
    double          time = t0;

    // sanity check for overly large dt inputs
    if (time + dt >= tf)
      dt = tf - time;

    while (time < tf) {

      bool last_step = false;
      if (time + dt >= tf) {
        dt        = tf - time;
        last_step = true;
      }

      for (int istage = 0; istage < n_stages; ++istage) {

        const double    time_at_stage = time + c[istage] * dt;
        Eigen::VectorXd u_at_stage    = u;

        for (int butchercol = 0; butchercol < istage; ++butchercol) {
          u_at_stage += dt * A(istage - 1, butchercol) * k[istage - 1];
        }

        rhs(u_at_stage, time_at_stage, k[istage]);
      }

      for (int istage = 0; istage < n_stages; ++istage) {
        u += dt * b[istage] * k[istage];
      }

      time += dt;

      if (last_step)
        break;
    }

    return u;
  }

private:
  virtual Eigen::MatrixXd ButcherTableauA() const = 0;
  virtual Eigen::VectorXd ButcherTableauB() const = 0;
  virtual Eigen::VectorXd ButcherTableauC() const = 0;
};

class ForwardEuler : public ExplicitRungeKutta
{
private:
  Eigen::MatrixXd ButcherTableauA() const override { return Eigen::MatrixXd::Zero(1, 1); }
  Eigen::VectorXd ButcherTableauB() const override { return Eigen::VectorXd::Constant(1, 1.0); }
  Eigen::VectorXd ButcherTableauC() const override { return Eigen::VectorXd::Constant(1, 0.0); }
};

class RK4 : public ExplicitRungeKutta
{
private:
  Eigen::MatrixXd ButcherTableauA() const override
  {
    Eigen::MatrixXd A = Eigen::MatrixXd::Zero(4, 4);
    A(1, 0)           = 0.5;
    A(2, 1)           = 0.5;
    A(3, 2)           = 1.0;

    return A;
  }

  Eigen::VectorXd ButcherTableauB() const override
  {
    Eigen::VectorXd b = Eigen::VectorXd::Zero(4);
    b[0]              = 1. / 6.;
    b[1]              = 1. / 3.;
    b[2]              = 1. / 3.;
    b[3]              = 1. / 6.;
    return b;
  }

  Eigen::VectorXd ButcherTableauC() const override
  {
    Eigen::VectorXd c = Eigen::VectorXd::Zero(4);
    c[1]              = 0.5;
    c[2]              = 0.5;
    c[3]              = 1.0;
    return c;
  }
};

class RK2Midpoint : public ExplicitRungeKutta
{
private:
  Eigen::MatrixXd ButcherTableauA() const override
  {
    Eigen::MatrixXd A = Eigen::MatrixXd::Zero(2, 2);
    A(1, 0)           = 1.0;
    return A;
  }

  Eigen::VectorXd ButcherTableauB() const override
  {
    Eigen::VectorXd b = Eigen::VectorXd::Zero(2);
    b[0]              = 0.5;
    b[1]              = 0.5;
    return b;
  }

  Eigen::VectorXd ButcherTableauC() const override
  {
    Eigen::VectorXd c = Eigen::VectorXd::Zero(2);
    c[1]              = 1.0;
    return c;
  }
};

class RK3SSP : public ExplicitRungeKutta
{
private:
  Eigen::MatrixXd ButcherTableauA() const override
  {
    Eigen::MatrixXd A = Eigen::MatrixXd::Zero(3, 3);
    A(1, 0)           = 1.0;
    A(2, 0)           = 0.25;
    A(2, 1)           = 0.25;
    return A;
  }

  Eigen::VectorXd ButcherTableauB() const override
  {
    Eigen::VectorXd b = Eigen::VectorXd::Zero(3);
    b[0]              = 1. / 6.;
    b[1]              = 1. / 6.;
    b[2]              = 2. / 3.;
    return b;
  }

  Eigen::VectorXd ButcherTableauC() const override
  {
    Eigen::VectorXd c = Eigen::VectorXd::Zero(3);
    c[1]              = 1.0;
    c[2]              = 0.5;
    return c;
  }
};

class RK3Ralston : public ExplicitRungeKutta
{
private:
  Eigen::MatrixXd ButcherTableauA() const override
  {
    Eigen::MatrixXd A = Eigen::MatrixXd::Zero(3, 3);
    A(1, 0)           = 0.5;
    A(2, 1)           = 0.75;
    return A;
  }

  Eigen::VectorXd ButcherTableauB() const override
  {
    Eigen::VectorXd b = Eigen::VectorXd::Zero(3);
    b[0]              = 2. / 9.;
    b[1]              = 1. / 3.;
    b[2]              = 4. / 9.;
    return b;
  }

  Eigen::VectorXd ButcherTableauC() const override
  {
    Eigen::VectorXd c = Eigen::VectorXd::Zero(3);
    c[1]              = 0.5;
    c[2]              = 0.75;
    return c;
  }
};

double
relative_error(double expected, double computed)
{
  return abs(computed / expected - 1.0);
}

int
main()
{
  plotScalarODEExactSolution();

  const Eigen::VectorXd u0 = Eigen::VectorXd::Constant(1, 1.0);
  const double          t0 = 0;
  const double          tf = 1;

  const double exact_soln = scalarODEExactSolution(tf);

  const std::vector<double> timestepsize{ 1. / 100., 1. / 1000., 1. / 10000. };
  std::vector<double>       rk4_err;
  std::vector<double>       rk3ssp_err;
  std::vector<double>       rk3ralston_err;
  std::vector<double>       rk2_err;
  std::vector<double>       forward_euler_err;

  for (const auto dt : timestepsize) {
    forward_euler_err.push_back(
      relative_error(exact_soln, ForwardEuler().solve(scalarODERightHandSide, u0, t0, tf, dt)[0]));
    rk4_err.push_back(
      relative_error(exact_soln, RK4().solve(scalarODERightHandSide, u0, t0, tf, dt)[0]));
    rk3ssp_err.push_back(
      relative_error(exact_soln, RK3SSP().solve(scalarODERightHandSide, u0, t0, tf, dt)[0]));
    rk3ralston_err.push_back(
      relative_error(exact_soln, RK3Ralston().solve(scalarODERightHandSide, u0, t0, tf, dt)[0]));
    rk2_err.push_back(
      relative_error(exact_soln, RK2Midpoint().solve(scalarODERightHandSide, u0, t0, tf, dt)[0]));
  }

  plt::figure();
  plt::named_loglog("RK1 (Forward Euler)", timestepsize, forward_euler_err, "-o");
  plt::named_loglog("RK2", timestepsize, rk2_err, "-o");
  plt::named_loglog("RK3 SSP", timestepsize, rk3ssp_err, "-o");
  plt::named_loglog("RK3 Ralston", timestepsize, rk3ralston_err, "-o");
  plt::named_loglog("RK4", timestepsize, rk4_err, "-o");
  plt::legend();
  plt::grid(true);
  plt::xlabel("$\\Delta t$");
  plt::ylabel("Error");
  plt::show();
}

```

</details>

