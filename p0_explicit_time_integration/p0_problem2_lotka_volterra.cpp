#include <Eigen/Dense>
#include <cmath>
#include <matplotlibcpp.h>
#include <memory>
#include <vector>

#include "TimeIntegrator/ForwardEuler.hpp"
#include "TimeIntegrator/RK2Midpoint.hpp"
#include "TimeIntegrator/RK3SSP.hpp"
#include "TimeIntegrator/RK3Ralston.hpp"
#include "TimeIntegrator/RK4.hpp"

namespace plt     = matplotlibcpp;
using RHSFunction = std::function<void(const Eigen::VectorXd&, double, Eigen::VectorXd&)>;


double
relative_error(double expected, double computed)
{
  return abs(computed / expected - 1.0);
}

double
lotkaVolterraConservedConstant(const Eigen::VectorXd& u)
{
  constexpr double alpha = 1.1;
  constexpr double beta  = 0.4;
  constexpr double gamma = 0.1;
  constexpr double delta = 0.4;
  return delta * u[0] - gamma * log(u[0]) + beta * u[1] - alpha * log(u[1]);
}

void
lotkaVolterraRightHandSide(const Eigen::VectorXd& u, double t, Eigen::VectorXd& dudt)
{
  constexpr double alpha = 1.1;
  constexpr double beta  = 0.4;
  constexpr double gamma = 0.1;
  constexpr double delta = 0.4;
  assert(u.size() == 2);
  dudt.resize(u.size());
  dudt[0] = alpha * u[0] - beta * u[0] * u[1];
  dudt[1] = delta * u[0] * u[1] - gamma * u[1];
}


void
solveProblem2LotkaVolterraTimeStepSweep()
{
  constexpr double t0 = 0;
  constexpr double tf = 1000;
  Eigen::VectorXd  u0 = Eigen::VectorXd::Constant(2, 10.0);
  const double     V0 = lotkaVolterraConservedConstant(u0);

  const std::vector<double> timestepsize{ tf / 1.e5, tf / 1.e6, tf / 1.e7 };
  std::vector<double>       rk4_err;
  std::vector<double>       rk3ssp_err;
  std::vector<double>       rk3ralston_err;
  std::vector<double>       rk2_err;
  std::vector<double>       forward_euler_err;

  for (const auto dt : timestepsize) {
    forward_euler_err.push_back(relative_error(
      V0,
      lotkaVolterraConservedConstant(
        ForwardEuler().solve(lotkaVolterraRightHandSide, u0, t0, tf, dt))));
    rk2_err.push_back(relative_error(
      V0,
      lotkaVolterraConservedConstant(
        RK2Midpoint().solve(lotkaVolterraRightHandSide, u0, t0, tf, dt))));
    rk3ssp_err.push_back(relative_error(
      V0,
      lotkaVolterraConservedConstant(RK3SSP().solve(lotkaVolterraRightHandSide, u0, t0, tf, dt))));
    rk3ralston_err.push_back(relative_error(
      V0,
      lotkaVolterraConservedConstant(
        RK3Ralston().solve(lotkaVolterraRightHandSide, u0, t0, tf, dt))));
    rk4_err.push_back(relative_error(
      V0, lotkaVolterraConservedConstant(RK4().solve(lotkaVolterraRightHandSide, u0, t0, tf, dt))));
  }

  plt::figure();
  plt::named_loglog("RK1 (Forward Euler)", timestepsize, forward_euler_err, "-o");
  plt::named_loglog("RK2", timestepsize, rk2_err, "-o");
  plt::named_loglog("RK3 SSP", timestepsize, rk3ssp_err, "-o");
  plt::named_loglog("RK3 Ralston", timestepsize, rk3ralston_err, "-o");
  plt::named_loglog("RK4", timestepsize, rk4_err, "-o");
  plt::legend();
  plt::grid(true);
  plt::xlabel("$\\Delta t$");
  plt::ylabel("Relative error in $V$");
}

void
solveProblem2LotkaVolterraPlotSolutionOverTime()
{
  constexpr double t0 = 0;
  constexpr double tf = 1000;
  Eigen::VectorXd  u0 = Eigen::VectorXd::Constant(2, 10.0);
  const double     V0 = lotkaVolterraConservedConstant(u0);

  std::vector<double> rk4_time_history;
  std::vector<double> rk4_soln_u0_history;
  std::vector<double> rk4_soln_u1_history;
  std::vector<double> rk4_conservation_history;

  std::unique_ptr<ExplicitRungeKutta> rk4 = std::make_unique<RK4>();

  auto rk4_observer = [&](int iter, double time, const Eigen::VectorXd& u) {
    rk4_time_history.push_back(time);
    rk4_soln_u0_history.push_back(u[0]);
    rk4_soln_u1_history.push_back(u[1]);
    rk4_conservation_history.push_back(lotkaVolterraConservedConstant(u));
  };

  rk4->attach(rk4_observer);

  const double dt = tf / 1.e5;

  rk4->solve(lotkaVolterraRightHandSide, u0, t0, tf, dt);

  plt::figure();
  plt::suptitle("RK4 with $dt = t_f / 10^5$");
  plt::subplot(3, 1, 1);
  plt::named_plot("Prey ($u_0$)", rk4_time_history, rk4_soln_u0_history);
  plt::legend();
  plt::grid(true);
  plt::subplot(3, 1, 2);
  plt::named_plot("Predators ($u_1$)", rk4_time_history, rk4_soln_u1_history);
  plt::legend();
  plt::grid(true);
  plt::subplot(3, 1, 3);
  plt::named_plot("Conserved variable $V$", rk4_time_history, rk4_conservation_history);
  plt::legend();
  plt::grid(true);

  plt::xlabel("$t$");
}

void
solveProblem2LotkaVolterraPlotConservedErrorOverTime()
{
  constexpr double t0 = 0;
  constexpr double tf = 1000;
  Eigen::VectorXd  u0 = Eigen::VectorXd::Constant(2, 10.0);
  const double     V0 = lotkaVolterraConservedConstant(u0);

  std::vector<double> rk1_time_history;
  std::vector<double> rk1_Verr;
  std::vector<double> rk2_time_history;
  std::vector<double> rk2_Verr;
  std::vector<double> rk3_time_history;
  std::vector<double> rk3_Verr;
  std::vector<double> rk4_time_history;
  std::vector<double> rk4_Verr;

  std::unique_ptr<ExplicitRungeKutta> rk1 = std::make_unique<ForwardEuler>();
  std::unique_ptr<ExplicitRungeKutta> rk2 = std::make_unique<RK2Midpoint>();
  std::unique_ptr<ExplicitRungeKutta> rk3 = std::make_unique<RK3SSP>();
  std::unique_ptr<ExplicitRungeKutta> rk4 = std::make_unique<RK4>();

  rk1->attach([&](int iter, double time, const Eigen::VectorXd& u) {
    rk1_time_history.push_back(time);
    rk1_Verr.push_back(relative_error(V0, lotkaVolterraConservedConstant(u)));
  });
  rk2->attach([&](int iter, double time, const Eigen::VectorXd& u) {
    rk2_time_history.push_back(time);
    rk2_Verr.push_back(relative_error(V0, lotkaVolterraConservedConstant(u)));
  });
  rk3->attach([&](int iter, double time, const Eigen::VectorXd& u) {
    rk3_time_history.push_back(time);
    rk3_Verr.push_back(relative_error(V0, lotkaVolterraConservedConstant(u)));
  });
  rk4->attach([&](int iter, double time, const Eigen::VectorXd& u) {
    rk4_time_history.push_back(time);
    rk4_Verr.push_back(relative_error(V0, lotkaVolterraConservedConstant(u)));
  });

  const double dt = tf / 1.e6;

  rk1->solve(lotkaVolterraRightHandSide, u0, t0, tf, dt);
  rk2->solve(lotkaVolterraRightHandSide, u0, t0, tf, dt);
  rk3->solve(lotkaVolterraRightHandSide, u0, t0, tf, dt);
  rk4->solve(lotkaVolterraRightHandSide, u0, t0, tf, dt);

  plt::figure();

  plt::named_semilogy("RK1 (Forward Euler)", rk1_time_history, rk1_Verr);
  plt::named_semilogy("RK2", rk2_time_history, rk2_Verr);
  plt::named_semilogy("RK3 SSP", rk3_time_history, rk3_Verr);
  plt::named_semilogy("RK4", rk4_time_history, rk4_Verr);

  plt::legend();
  plt::grid(true);

  plt::xlabel("$t$");
  plt::ylabel("Relative error in $V$");
}


int
main()
{
  solveProblem2LotkaVolterraTimeStepSweep();
  solveProblem2LotkaVolterraPlotSolutionOverTime();
  solveProblem2LotkaVolterraPlotConservedErrorOverTime();

  plt::show();
}
