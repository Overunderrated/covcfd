# Problem 2 part 2


- [Problem 1: An initial pass](p0_problem1.md)
- [Problem 1: Refactoring the time integrators](p0_problem1_refactoring.md)
- [Problem 1: Refactoring the time integrators part 2](p0_problem1_refactoring2.md)
- [Problem 2: An initial pass](p0_problem2.md)
- [Problem 2: Implementing the observer pattern](p0_problem2_observer.md)
- [Problem 2: Cleanup](p0_problem2_cleanup.md)


Run
```
git checkout v0.2.2
```
to checkout the code at the end of this section.

## Implementing the observer pattern

At this point we still only get the solution at the final time, when we're interested in the evolution of it throughout time. For this we'll use a common design pattern called the [observer pattern](https://en.wikipedia.org/wiki/Observer_pattern) with some guidance from [this stackexchange post.](https://codereview.stackexchange.com/questions/184702/implementation-of-observer-pattern-in-c14)

The observer pattern uses [*callback*](https://en.wikipedia.org/wiki/Callback_(computer_programming)) functions; the user of the observable class (the class to-be-observed) *attaches* some function to the observable, which then calls all its attached observers, usually when some particular *event* occurs.

This is a pretty common design even in numerics code -- e.g. matlab allows registering callbacks via the `odeset` options.

In this case, we want our time integrators to call some function to let them know the current status:
- current iteration number
- current time
- current solution

In order to do this we'll define:
- `ObserverType`, the type of the observer function: in this case a function that takes the above arguments describing the status
- an `attach` function allowing users to attach observers
- an `std::vector<ObserverType>` private member variable in the time integrator to store those observers
- calls to all the attached observers (and there may be zero or arbitrarily many observers).

class `ExplicitRungeKutta` is modified as below:

```c++
class ExplicitRungeKutta
{
public:
  using ObserverType = std::function<void(int, double, const Eigen::VectorXd&)>;
  void attach(ObserverType o) { observers.push_back(o); }

  Eigen::VectorXd
  solve(const RHSFunction& rhs, const Eigen::VectorXd& u0, double t0, double tf, double dt)
  {
    // ...
    while (time < tf) {

      // ... step forward in time

      // call the observers
      for (auto& o : observers){
        o(iter, time, u);
      }

      if (last_step)
        break;
    }
    return u;
  }

private:
  // ...

  std::vector<ObserverType> observers;
};
```

At this point the `observable` doesn't do anything; we need to attach `observers`. As in the previous part set up the initial values and constants, and then also declare some `vector`s to store the time history in, as well as instantiate an object of RK4 (previously we didn't need to keep the integrator objects around any longer than was needed to call `.solve()`).

```c++
void solveProblem2LotkaVolterraPlotSolutionOverTime()
{
  constexpr double t0 = 0;
  constexpr double tf = 1000;
  Eigen::VectorXd  u0 = Eigen::VectorXd::Constant(2, 10.0);
  const double     V0 = lotkaVolterraConservedConstant(u0);

  std::vector<double> rk4_time_history;
  std::vector<double> rk4_soln_u0_history;
  std::vector<double> rk4_soln_u1_history;
  std::vector<double> rk4_conservation_history;
  
  std::unique_ptr<ExplicitRungeKutta> rk4 = std::make_unique<RK4>();
```

Now some C++11 magic comes -- lambda functions make implementing observers on the spot trivial. We create one that stores the solution as it comes in from the integrator:

```c++
  auto rk4_observer = [&](int iter, double time, const Eigen::VectorXd& u) {
    rk4_time_history.push_back(time);
    rk4_soln_u0_history.push_back(u[0]);
    rk4_soln_u1_history.push_back(u[1]);
    rk4_conservation_history.push_back(lotkaVolterraConservedConstant(u));
  };
```

and then attach it to the integrator and then run it.

```c++
  rk4->attach(rk4_observer);

  const double dt = tf / 1.e5;
  rk4->solve(lotkaVolterraRightHandSide, u0, t0, tf, dt);
```

now the `std::vector`s defined above will be filled when the time integrator automatically calls all its observers (the lambda function `rk4_observer` above) which itself appends the data to the `vector`s.

Then we can plot it, and hopefully you see something like this:

![alt text](lotkavolterra_timehistory_rk4.png)

Neat. As for the solution, you should notice right away that the peaks of the solution variables are continually increasing, and likewise the conserved variable `$V$` which *should* be constant is monotonically increasing.


Also note that there's no need to store the temporary lambda observer.
```c++
  auto rk4_observer = [&](int iter, double time, const Eigen::VectorXd& u) {
    rk4_time_history.push_back(time);
    rk4_soln_u0_history.push_back(u[0]);
    rk4_soln_u1_history.push_back(u[1]);
    rk4_conservation_history.push_back(lotkaVolterraConservedConstant(u));
  };
  rk4->attach(rk4_observer);
```

can be directly replaced by

```c++
  rk4->attach([&](int iter, double time, const Eigen::VectorXd& u) {
    rk4_time_history.push_back(time);
    rk4_soln_u0_history.push_back(u[0]);
    rk4_soln_u1_history.push_back(u[1]);
    rk4_conservation_history.push_back(lotkaVolterraConservedConstant(u));
  };);
```



<details>
<summary><b>Click to expand the full function that creates the above plot</b></summary>

```c++
void solveProblem2LotkaVolterraPlotSolutionOverTime()
{
  constexpr double t0 = 0;
  constexpr double tf = 1000;
  Eigen::VectorXd  u0 = Eigen::VectorXd::Constant(2, 10.0);
  const double     V0 = lotkaVolterraConservedConstant(u0);

  std::vector<double> rk4_time_history;
  std::vector<double> rk4_soln_u0_history;
  std::vector<double> rk4_soln_u1_history;
  std::vector<double> rk4_conservation_history;

  std::unique_ptr<ExplicitRungeKutta> rk4 = std::make_unique<RK4>();

  auto rk4_observer = [&](int iter, double time, const Eigen::VectorXd& u) {
    rk4_time_history.push_back(time);
    rk4_soln_u0_history.push_back(u[0]);
    rk4_soln_u1_history.push_back(u[1]);
    rk4_conservation_history.push_back(lotkaVolterraConservedConstant(u));
  };

  rk4->attach(rk4_observer);

  const double dt = tf / 1.e5;

  rk4->solve(lotkaVolterraRightHandSide, u0, t0, tf, dt);

  plt::figure();
  plt::suptitle("RK4 with $dt = t_f / 10^5$");
  plt::subplot(3, 1, 1);
  plt::named_plot("Prey ($u_0$)", rk4_time_history, rk4_soln_u0_history);
  plt::legend();
  plt::grid(true);
  plt::subplot(3, 1, 2);
  plt::named_plot("Predators ($u_1$)", rk4_time_history, rk4_soln_u1_history);
  plt::legend();
  plt::grid(true);
  plt::subplot(3, 1, 3);
  plt::named_plot("Conserved variable $V$", rk4_time_history, rk4_conservation_history);
  plt::legend();
  plt::grid(true);

  plt::xlabel("$t$");
}

int main()
{
  // solveProblem1ScalarODE();
  // solveProblem2LotkaVolterraTimeStepSweep();
  solveProblem2LotkaVolterraPlotSolutionOverTime();

  plt::show();
}
```

</details>

You should see something like this:

![alt text](lotkavolterra_conservationvstime.png)

Again the RK3SSP method is punching above its weight, and RK2 is right on top of forward Euler.
