#pragma once

#include <functional>
#include <vector>
#include <Eigen/Dense>

using RHSFunction = std::function<void(const Eigen::VectorXd&, double, Eigen::VectorXd&)>;

class ExplicitRungeKutta
{
public:
  using ObserverType = std::function<void(int, double, const Eigen::VectorXd&)>;
  void attach(ObserverType o) { observers.push_back(o); }

  Eigen::VectorXd
  solve(const RHSFunction& rhs, const Eigen::VectorXd& u0, double t0, double tf, double dt)
  {
    const auto A        = ButcherTableauA();
    const auto b        = ButcherTableauB();
    const auto c        = ButcherTableauC();
    const int  n_stages = A.rows();

    std::vector<Eigen::VectorXd> k(n_stages);

    Eigen::VectorXd u    = u0;
    double          time = t0;
    int             iter = 0;

    // sanity check for overly large dt inputs
    if (time + dt >= tf)
      dt = tf - time;

    while (time < tf) {

      bool last_step = false;
      if (time + dt >= tf) {
        dt        = tf - time;
        last_step = true;
      }

      for (int istage = 0; istage < n_stages; ++istage) {

        const double    time_at_stage = time + c[istage] * dt;
        Eigen::VectorXd u_at_stage    = u;

        for (int butchercol = 0; butchercol < istage; ++butchercol) {
          u_at_stage += dt * A(istage - 1, butchercol) * k[istage - 1];
        }

        rhs(u_at_stage, time_at_stage, k[istage]);
      }

      for (int istage = 0; istage < n_stages; ++istage) {
        u += dt * b[istage] * k[istage];
      }

      time += dt;
      iter++;

      for (auto& o : observers) {
        o(iter, time, u);
      }

      if (last_step)
        break;
    }

    return u;
  }

private:
  virtual Eigen::MatrixXd ButcherTableauA() const = 0;
  virtual Eigen::VectorXd ButcherTableauB() const = 0;
  virtual Eigen::VectorXd ButcherTableauC() const = 0;

  std::vector<ObserverType> observers;
};
